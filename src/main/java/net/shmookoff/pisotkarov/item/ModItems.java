package net.shmookoff.pisotkarov.item;

import net.shmookoff.pisotkarov.PisotKarov;

import java.util.function.Supplier;

import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItems {
        private static Supplier<? extends Item> supplier() {
                return () -> new Item(new Item.Properties().tab(ModCreativeModeTab.PISOTKAROV_TAB));
        }

        public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
                        PisotKarov.MOD_ID);

        public static final RegistryObject<Item> ADIN_KAROV = ITEMS.register("adin_karov",
                        supplier());
        public static final RegistryObject<Item> PYAT_KAROV = ITEMS.register("pyat_karov",
                        supplier());
        public static final RegistryObject<Item> DESIT_KAROV = ITEMS.register("desit_karov",
                        supplier());
        public static final RegistryObject<Item> PISAT_KAROV = ITEMS.register("pisyat_karov",
                        supplier());
        public static final RegistryObject<Item> STO_KAROV = ITEMS.register("sto_karov",
                        supplier());
        public static final RegistryObject<Item> PISOT_KAROV = ITEMS.register("pisot_karov",
                        supplier());

        public static void register(IEventBus eventBus) {
                ITEMS.register(eventBus);
        }
}
