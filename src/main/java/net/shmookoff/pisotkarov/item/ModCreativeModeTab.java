package net.shmookoff.pisotkarov.item;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModCreativeModeTab {
    public static final CreativeModeTab PISOTKAROV_TAB = new CreativeModeTab("pisotkarovtab") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ModItems.PISOT_KAROV.get());
        }
    };
}
